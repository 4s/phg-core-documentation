
Introduction     {#mainpage}
============

\if false

    ******************************************************************************
    ***                                                                        ***
    ***     This file contains the first (and main) page of the 4S PHG core    ***
    ***     documentation, with the introduction and documentation overview.   ***
    ***                                                                        ***
    ***        AUTHOR:   Jacob Andersen (C) 2018 4S / The Alexandra Institute  ***
    ***        LICENSE:  Apache 2.0                                            ***
    ***                                                                        ***
    ******************************************************************************

\endif


The 4S PHG core module collection is a cross platform library for
*personal health gateway* (PHG) applications interacting with
*personal health devices* (PHD) and *health & fitness servers* (HFS).

\if full

This manual is aimed at contributors to the PHG core module
collection, and contains the full and hairy implementation details.

\else 

This manual is aimed at developers who are either going to use modules
from the 4S PHG core module collection or need a general technical
overview of the 4S PHG (core) architecture. The content of this manual
will therefore not cover implementation details.

\endif


\htmlonly
This manual is also available as a PDF file.
\endhtmlonly
\latexonly
This manual is also available as interactive web pages.
\endlatexonly
Please refer to [www.4s-online.dk/PHG/core]
(http://www.4s-online.dk/PHG/core)
for the relevant documentation.


\if full

\note
If you are not intending to modify any of the 4S PHG core modules, you
are encouraged instead to look at the *Interface Documentation*, which
is aimed at developers who are only going to use the modules but have
no need for the internal implementation details (as well as other
people, who just need the essential overview). The *Interface
Documentation* is also the recommended place to start for new
contributors, who wish to get an overview of the 4S PHG core module
library without too much "noise".

\note
The latest version of the *Interface Documentation* can be found here:

\note
[www.4s-online.dk/PHG/core/latest/interface/]
(http://www.4s-online.dk/PHG/core/latest/interface/)

\else

\note
Developers already familiar with the architecture, who are planning to
contribute to the 4S PHG core modules, are encouraged to head over to
the *Full Documentation*, which contains all the hairy implementation
details.

\note
The latest version of the *Full Documentation* can be found here:

\note
[www.4s-online.dk/PHG/core/latest/full/]
(http://www.4s-online.dk/PHG/core/latest/full/)

\endif


Documentation Overview     {#sec_overview}
======================

\htmlonly
This manual is organised with a number of introductory pages followed
by detailed information about each of the modules, classes and
files. The pages can be navigated by using the tree-view on the left
of your screen (and of course by following the hyperlinks). If you
wish to print a hardcopy of the documentation instead, we suggest
printing from the PDF file instead, as it is optimised for printing.
\endhtmlonly
\latexonly
This manual is organised with a number of introductory chapters
followed by chapters with detailed information about each of the
modules, classes and files. The detailed information in the last
(huge) chapters may be easier to navigate on the interactive web pages
version of the manual.
\endlatexonly

If you are new to the world of 4S PHG core, we recommend that you read
the \ref page_start "Getting Started"
\htmlonly
page, followed by the pages
\endhtmlonly
\latexonly
chapter on page \pageref{page_start}, and continue through the
following chapters
\endlatexonly
about the 4S PHG background and architecture.


Other Sources of Information     {#sec_external_docs}
============================

The documentation found
\htmlonly
on these pages
\endhtmlonly
\latexonly
in this document
\endlatexonly
is generated from the source code of a certain version of the 4S PHG
core library, and is therefore rather static in nature. However, there
are a few other sources of information, you should be aware of --
all of them are more dynamic, and of course you are invited to
contribute as well:

\if false
***************
** FIXME: Add link to wiki when the project is listed here
***************
\addindex Wiki
<dl><dt>Wiki</dt>
<dd>The 4SDC Wiki page at
[4s-online.dk/wiki/doku.php?id=4sdc:]
(http://4s-online.dk/wiki/doku.php?id=4sdc:) is used first of
all as a general overview over the 4SDC module collection and secondly
as the tutorial on how to get the demo application up and running -- on
various platforms.</dd>
\endif

\addindex Issuetracker
\addindex Jira
<dl><dt>Issuetracker</dt>
<dd>The issuetracker at [issuetracker4s.atlassian.net/browse/PM/]
(https://issuetracker4s.atlassian.net/browse/PM/) is used to report
bugs and feature requests, and to coordinate the work on the 4S PHG
modules.</dd>
\addindex Forum
<dt>Forum</dt>
<dd>The forum at [4s-online.dk/forum/] (http://4s-online.dk/forum/) is
used for Q/A and discussions.</dd></dl>

\if false
******************************************************************************
***                                                                        ***
***    Below the order of the remaining chapters of the documentation      ***
***    is defined                                                          ***
***                                                                        ***
******************************************************************************
\endif


\page      page_start             Getting Started
\page      page_background        Background
\page      page_overall_arch      4S PHG Overall Architecture
\page      page_core_arch         4S PHG Core Architecture
\page      page_baseplate_arch    4S PHG Core Baseplate Architecture
\page      page_messages_arch     4S PHG Core Messages Architecture

\if full
\page      page_conformance       Conformance Information
\endif

\page      page_license           License
\page      page_contributors      Contributors

\if false
***   SHOULD WE INCLUDE A TERMINOLOGI / DICTIONARY PAGE??   ***
\endif


\defgroup  group_baseplate        Baseplate Library
\defgroup  group_baseplate_native Native Baseplate Library
\ingroup   group_baseplate
\defgroup  group_baseplate_jvm    Java VM Baseplate Library
\ingroup   group_baseplate
\defgroup  group_baseplate_cli    CLI Baseplate Library
\ingroup   group_baseplate
\defgroup  group_baseplate_js     JavaScript Baseplate Library
\ingroup   group_baseplate

\defgroup  group_pal              Platform Abstraction Layer
\defgroup  group_pal_interfaces   PAL Interfaces
\ingroup   group_pal
\defgroup  group_pal_android      Android Platform Specific Modules
\ingroup   group_pal
\defgroup  group_pal_ios          iOS Platform Specific Modules
\ingroup   group_pal

\defgroup  group_core             Core Platform Independent Library
\defgroup  group_core_interfaces  API Interfaces
\ingroup   group_core
