
4S PHG Core Architecture     {#page_core_arch}
========================

\if false

    ******************************************************************************
    ***                                                                        ***
    ***     This file is part of the 4S PHG core documentation, and contains   ***
    ***     the page/chapter with the 4S PHG Core architecture intro.          ***
    ***                                                                        ***
    ***        AUTHOR:   Jacob Andersen (C) 2020 4S / The Alexandra Institute  ***
    ***        LICENSE:  Apache 2.0                                            ***
    ***                                                                        ***
    ******************************************************************************

\endif


[TOC]

The key idea behind the 4S PHG core library is to create a "microservices light" environment that can be used
in a portable/platform independent manner on a wide range of different ressource-constrained systems from
embedded microcontrollers to smartphones.

Among the architectural principles (see the [background](\ref page_background)
\htmlonly
page),
\endhtmlonly
\latexonly
chapter),
\endlatexonly
we find the need to partition the software system into modules, that are *independent* and
*self-contained*. This is important in order to support having multiple vendors (authors of the modules) as
well as for isolating critical elements of the software for regulatory acceptance. For more background on the
4S model for regulatory compliance and quality assurance, please read the "Model for kvalitetssikring"
document \cite kvalitetssikring, which, unfortunately, is only available in Danish.

In summary, this model defines an ecosystem of independently developed modules capable of collaborating to form a
larger system, much like a large number of small narrowly-focused applications comprise the UNIX operating
system. This model is illustrated using a LEGO analogy: very different designs can be created using a small
set of LEGO bricks assembled on top of a LEGO baseplate.

The key to LEGO's success is the ability to create a large number of different designs using the same bricks
and baseplates because all bricks connect to each other and the baseplate using a common "interface". The 4S
regulatory compliance model leverages the same basic idea: defining the common "shape" of all modules and how
they will be interfacing to one another as well as a "baseplate".


PHG Core Architecture Overview
==============================

The "microservices light" approach is little more than taking the existing well-known microservices
architectural style, scaling it down to a shape, that will fit onto a small computing device, while ensuring
that we do lot loose some of the key benefits of this architectural style:

 * Self-contained modules
 * Isolation between modules --- critical to regulatory compliance as outlined above
 * Loose coupling


Modules and Threads
-------------------

In conventional microservice systems, the central software unit is a (Docker) container. In the 4S PHG core
architecture the equivalent unit is the *core module*, which offers a way to package and distribute a narrow
functionality that is loosely coupled to other modules as a single binary library, which just has to be pulled
in and linked to the application. The different types of modules were introduced in section \ref
sec_birdsperspective.

> A **core module** is a unit of software that is independently replaceable and upgradeable. It is created,
> handles and generates messages, and is eventually destroyed on **one** single thread. The only public API
> handles of a *pure* core module are the constructor and destructor, and all the module's interaction with
> other modules or the application goes through the baseplate message bus.

A *mixed core* module will offer a public API to the application (and should be careful about any
multi-threading issues in doing so). A *platform abstraction core* module may internally use multiple threads.

Notice that this statement expresses that a pure core module shall never worry about multi-threading issues as
they are guaranteed *not* to arise. The statement does not imply that all pure core modules will each get
their own private thread. Although this is probably the common case on "powerful" platforms and operating
systems, the underlying platform *may* be single threaded, or may offer only a limited total number of
threads. Therefore all pure core modules must use event-based asynchronous / non-blocking implementations
exclusively --- i.e. they respond to a message by performing some non-blocking work, perhaps including sending
some messages, and then return as soon as possible.


Message Bus, Interfaces, and the Baseplate
------------------------------------------

The communication between modules flows over the message bus, which is one of the services offered by the
baseplate. Modules must declare the interfaces they *produce* or *consume*, and the baseplate will ensure that
all consumers of a particular interface will communicate with all producers of the same interface.

Two forms of communication are supported (an interface can use one or both, and they can be used in either
direction):

 * Asynchronous (unsolicited) publishing of events (unicast or multicast).
 * Synchronous procedure call (unicast only, with response).
 
Furthermore, there is a common well-defined way of communicating object instances between modules. This,
together with the remote procedure calls may be used to perform remote method invocation on objects from one
module to another. 

Further details on the message bus is presented
\htmlonly
on page
\endhtmlonly
\latexonly
in chapter
\endlatexonly
\ref page_messages_arch.

In addition to passing around messages, the baseplate has the following responsibilities:

 * Starting up the modules and reporting to the application when all modules are started and ready
 * Closing down the modules in an orderly fashion (that may require interaction between the modules).
 * Handling global events and informing modules about them (e.g. suspend/resume events).
 * Collecting log output from the modules.
 * Catching (and perhaps reporting) unhandled errors in modules
 
 More details on the baseplate is presented
\htmlonly
on page
\endhtmlonly
\latexonly
in chapter
\endlatexonly
\ref page_baseplate_arch.

\latexonly
\begin{figure}\centering
\includegraphics[page=1,width=.4\textwidth]{core_arch_overview.pdf}
\caption{\label{fig:corearchoverview}Overall PHG Core architecture.}
\end{figure}
\begin{comment}
% a hack to tell Doxygen to copy the image file
\endlatexonly
\htmlonly
<div class="image" style="float: right; margin-left: 30px;
 margin-bottom: 30px; width: 40%;">
<img style="width: 100%;" src="core_arch_overview.svg" alt="">
<div class="caption">Overall PHG Core architecture.</div>
</div><div style="display:none; visibility:hidden;">
a hack to tell Doxygen to copy the image file
\endhtmlonly
\image latex core_arch_overview.pdf
\image html core_arch_overview.svg
\latexonly
\end{comment}
\endlatexonly
\htmlonly
</div>
\endhtmlonly

Platform Abstraction Layer
--------------------------

All platform specific core modules used in an application are collectively termed the *“Platform Abstraction
Layer”* (PAL). The purpose of these modules is to provide the pure core modules with access to external APIs,
such as the operating system.

The general idea is that a (platform independent) interface is defined for some purpose. This could be access
to a real-time clock, a store for configuration data, or access to USB or Bluetooth devices. This particular
interface can then be consumed by pure core modules that need them, and produced by platform specific
modules. One such platform specific module must therefore exist for each platform this will run on --- all
producing the exact same interface.

As an example
\htmlonly
the figure
\endhtmlonly
\latexonly
figure \ref{fig:corearchoverview}
\endlatexonly
shows that for each of the 5 operating systems, three platform specific modules have been implemented to
produce the three required interfaces (the three solid lines going between the "Library modules" and "Platform
Abstraction" groups).


Programming Languages             {#sec_programminglanguages}
---------------------

It has been decided that pure core modules *shall* be developed in C++11 and *should* limit their dependencies
to the standard libraries of C++11 only.

GUI-related mixed core modules will be developed in JavaScript (or TypeScript, which is transpiled into
JavaScript). At the moment there is no decision regarding external dependencies, except *minimize*.

Other mixed core modules as well as platform abstraction modules may be targeted environments executing on top
of a virtual machine. For instance, Android apps can run on the Java Virtual Machine, and Windows apps can run
on the Common Language Runtime (the virtual machine of .NET).


Application Framework Adapters    {#sec_applicationframeworkadaptors}
------------------------------

In order to ensure that using the 4S PHG core is easily accessible to a developer, it can be integrated
seamlessly in existing application frameworks, such as Apache Cordova, Qt, Flutter, or React Native. The
modules (and meta information about inter-dependencies between modules of the 4S PHG core) is published using
the tools and repositories of the application frameworks, so the application developers will need no special
steps or knowledge to include 4S PHG core modules in their app.

\htmlonly
The figure
\endhtmlonly
\latexonly
Figure \ref{fig:corearchoverview}
\endlatexonly
demonstrates how the application framework adaptors exist decoupled from the modules. An adapter relates
directly to the baseplate, and is responsible for the *piping* (see the 
[baseplate architecture](\ref page_baseplate_arch)
\htmlonly
page),
\endhtmlonly
\latexonly
chapter),
\endlatexonly
collecting logging data for the appropriate sink, and managing all dependencies --- including fetching all
modules and linking them into the final application.

*At the moment,* the only supported application framework is Apache Cordova. 








\if false

\todo Bonus information -- the following needs to be incorporated in the text at some point...




C++ er valgt, da:
  - Sproget er stabilt og ekstremt udbredt (og har bevist sit værd
    gennem mange år, og derfor må forventes at leve videre i mange år
    fremover).
  - Kan afvikles native på alle de listede platforme pånær Firefox OS
    (se dog næste punkt).
  - Kan kompileres (vha. Emscripten http://emscripten.org) til en
    delmængde af JavaScript (asm.js), som derefter kan afvikles på
    alle browser-platforme – og dette er tilmed effektivt. Dermed får
    man samtidigt alle fordelene fra JavaScript med.
  - I de tilfælde, hvor man både kører en del af 4SDC i native kode og
    en applikation i en browser, vil man kunne vælge, hvilke
    komponenter, der skal køre native og hvilke, der skal afvikles i
    browserens JavaScript motor. Således kan komponenter, der ofte
    ændres, loades dynamisk fra webserveren.


\endif
