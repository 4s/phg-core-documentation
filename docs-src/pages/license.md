
\page page_license

\if false
******************************************************************************
***                                                                        ***
***     This file is part of the 4S PHG core documentation, and contains   ***
***     the page/chapter with the information about the license terms.     ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) 2018 4S / The Alexandra Institute  ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif

\addindex License

The following sections list the license and copyright terms governing
the source code, documentation, and external libraries in the 4S PHG
Core Module Collection.

Source Code
===========
\addindex Apache 2.0 License

The 4S PHG Core Module Collection is released under the **Apache 2.0
license**.

A copy of the full license text can be found below and at
[www.apache.org/licenses/LICENSE-2.0]
(http://www.apache.org/licenses/LICENSE-2.0).

\verbinclude "../LICENSE"

Documentation
=============
\addindex Creative Commons Attribution 4.0
\addindex CC-BY-4.0

Documentation text in general is under **Creative Commons Attribution
4.0 International Public License** (a.k.a CC-BY-4.0), you can obtain a
copy of this license at [creativecommons.org/licenses/by/4.0/]
(http://creativecommons.org/licenses/by/4.0/)
