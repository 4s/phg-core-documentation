
4S PHG Core Baseplate Architecture     {#page_baseplate_arch}
==================================

\if false

    ******************************************************************************
    ***                                                                        ***
    ***     This file is part of the 4S PHG core documentation, and contains   ***
    ***     the page/chapter with the baseplate common architecture.           ***
    ***                                                                        ***
    ***        AUTHOR:   Jacob Andersen (C) 2020 4S / The Alexandra Institute  ***
    ***        LICENSE:  Apache 2.0                                            ***
    ***                                                                        ***
    ******************************************************************************

\endif


[TOC]

As outlined in previous sections, the purpose of the "baseplate" is to offer a lightweight microservices
environment for use on resource-constrained platforms. This includes isolating modules in individual
"containers" communicating through a common message bus.

The scope of the baseplate includes the following:

 * **Starting up and shutting down** all modules in a controlled way, so that no activities are started until
   all modules are ready, and so that the system does not exit until all modules have cleaned up --- and
   possibly saved their state.
 * **Provide the message bus** which is responsible for message delivery between modules.
 * **Logging and error handling** in order to assist the developer in tracking down problems.
\if false
 * Også: Håndtering af afhængigheder / API versionering o.l. (fx gennem NPM manifestfiler)
\endif

The following sections cover some of the main features of the baseplate.


ModuleBase     {#sec_modulebase}
==========

Modules are created by extending an abstract base class called `ModuleBase`. The implementation of a module
*shall* provide one class extending `ModuleBase`. This class *shall* expose one public constructor taking a
single argument of the type `Context` explained below. (Note that platform abstraction modules may require
additional arguments in the constructor signature).

For pure core modules this constructor *shall* be the *only* public interface (except a public destructor in
C++). Mixed core modules will offer an additional public API to be used by the application. For more about the
different types of modules, see the section on \ref sec_birdsperspective.


Multiple Implementations
========================

As explained in the section on \ref sec_programminglanguages, mixed core modules and platform abstraction
modules may need to execute in a virtual machine (Java, C# etc.) or on an interpreter (JavaScript). For this
reason, the baseplate has been implemented in multiple languages to allow a mixture of modules running in a
heterogonous environment:

 * The **native baseplate** is implemented in C++ and distributed as a dynamically-linked library for the
   native platform. Languages that are compiled to native libraries can be used to create modules that depend
   on the native baseplate. A C++ class extending the C++ version of the `ModuleBase` class must be provided,
   as explained in the previous section, but apart from that the choice of language is up to the module
   developer.
 * The **JVM baseplate** is implemented in Java and distributed as a `.jar` file. It is intended for use with
   modules written in the Java language, or other languages running on a Java Virtual Machine (JVM). An
   abstract Java version of the `ModuleBase` class is available here.
 * The **JS baseplate** is implemented in JavaScript and execute on a JavaScript interpreter. It can also be
   used with other languages running on a JS interpreter (such as TypeScript). A TypeScript-compatible
   JavaScript implementation of the `ModuleBase` class is available to be used from either JavaScript or
   TypeScript.

In the future, a baseplate for Microsoft's Common Language Runtime (the .NET virtual machine) may also be
added, but there are currently no plans for this.


Context
=======

The "glue" that binds all modules together is the baseplate's `Context` object. To the module developer, this
can be thought of as an opaque object that you will receive in the module's constructor and hand over to the
abstract base class. It exposes no "user-serviceable" parts.

When an application framework adaptor is used (c.f. the \ref sec_applicationframeworkadaptors section), it
will be the responsibility of this adaptor to create the `Context` objects as well as instantiating all the
modules. Therefore, the application developer using the PHG core modules through an application framework
adaptor (such as the Cordova Adaptor) will never come in direct contact with the `Context` objects.

A `Context` object represents the module's access to the message bus --- and thus the entire PHG core
system. There are two subtypes of the `Context` class:

   * `ReflectorContext`
   * `BridgeContext`

`ReflectorContext`
------------------

The overall purpose of the message bus is to relay messages to all relevant modules. Typically, one module
will be the producer of an interface. When messages are multicasted on that particular interface, all modules
that have registered as consumers of the same interface should receive a copy.

This is implemented using a central "reflector" component, which receives all messages sent from all modules,
and relays them individually to all the modules that have registered to receive the particular message type.

Obviously, only *one* instance of the reflector can exist on the entire PHG core system, and it must be
"alive" in order for the system to work. Therefore, if parts of the system can be awake while other parts are
asleep (e.g. a background service running while a foreground app is suspended), the reflector must exist in
the "awake" part the system.

`BridgeContext`
---------------

As explained in the previous section, the baseplate comprises multiple implementations in different languages,
and a PHG core-based application will typically use more than one of these. As (at least) one `Context` object
is needed for each baseplate implementation in use, and there can be only one reflector, a different type is
needed in all other cases. This type is the `BridgeContext`. Its purpose is simply to forward the
communication between all the modules connected to it and the global singleton `ReflectorContext` object. This
is done using the *piping* described in the next section.


\latexonly
\begin{figure}\centering
\includegraphics[page=1,width=.4\textwidth]{piping.pdf}
\caption{\label{fig:piping}The components involved in piping.}
\end{figure}
\begin{comment}
% a hack to tell Doxygen to copy the image file
\endlatexonly
\htmlonly
<div class="image" style="float: right; margin-left: 30px;
 margin-bottom: 30px; width: 40%;">
<img style="width: 100%;" src="piping.svg" alt="">
<div class="caption">The components involved in piping.</div>
</div><div style="display:none; visibility:hidden;">
a hack to tell Doxygen to copy the image file
\endhtmlonly
\image latex piping.pdf
\image html piping.svg
\latexonly
\end{comment}
\endlatexonly
\htmlonly
</div>
\endhtmlonly


Piping
======

\htmlonly
The figure
\endhtmlonly
\latexonly
Figure \ref{fig:piping}
\endlatexonly
illustrates the components involved in the *piping*, which is the mechanism used to tie the different
implementations of the baseplate together.

The yellow boxes represent 4 modules attached to two different contexts. It could be two native modules and
two JavaScript modules, but this is not important. The purple boxes represent components of the baseplate
implementations. And, finally, the teal box is an extra component tying the different implementations
together. When an application framework adaptor is used (c.f. the \ref sec_applicationframeworkadaptors
section), this component will be implemented by the adaptor.

The numbered arrows show a message flow, where module 3 broadcasts a message that will be received by all
modules (including itself).

The data flowing between the `BridgeContext` and `PipingModule` components will be primitive byte arrays that
can be easily communicated between different languages. The job of the bridge implementation is to carry these
byte arrays across the language boundaries.

One `PipingModule` is instantiated and attached to the `ReflectorContext` for each instance of the
`BridgeContext`. This `PipingModule` instance will subscribe to the union of all subscriptions of the modules
attached to its `BridgeContext`, and thus act as a proxy for all these modules towards the reflector.



`MasterModule` and application state
====================================

The baseplate maintains and manage the state of all connected modules from a `MasterModule`, which is a
special singleton module dedicated to orchestrating the system startup and shutdown, as well as announcing
certain global application state changes (such as suspend/resume).

When an application framework adaptor is used (c.f. the \ref sec_applicationframeworkadaptors section), it
will be the responsibility of this adaptor to create the `MasterModule`. Hence, the typical application
developer will not come in direct contact with this module.

\if false

Future work on the MasterModule:

baseplate system reflection (fx liste over kørende moduler; moduler, der er stoppet/gået ned; moduler, der
udstiller eller afhænger af en given snitflade; auto-svar på unicast beskeder til moduler, der er stoppet;
etc)

hot-pluggable modules

\endif


Logging and Error Handling     {#sec_errorhandling}
==========================

The `ModuleBase` class includes advanced logging primitives designed for centralized logging and the creation
of "stack traces" across module (and language) boundaries when errors occur. The "stack traces" are created by
chaining together messages of the `Error` type. This is a message type shared by the baseplate and all
interface definitions.

A message of the `Error` type contains

 * The name of the module creating the error
 * a short error message
 * source file and line number
 * optionally class and function name
 * optionally a long description (such as a "local" stack trace)
 * conditionally an interface name and error code (defined by the interface)
 * optionally another "caused-by" `Error` message

A log message can contain an `Error` message as one of its optional elements. This provides a common way to
output an error or warning with a cross-module trace.


Unhandled Errors
----------------

The baseplate will catch any uncaught errors occurring in the modules. This will raise a special situation
called a *core panic* --- an unhandled error caused the baseplate or a module to fail and likely shut down.

Whenever a core panic happens, it is evidence of a programming error. Some core panics will allow for the
thread it happened on to continue, but it is likely that the associated module will be in a bad state. Most
core panic types, however, will cause the thread to terminate, so the likely outcome is a dead module.

Catching core panics during development is key to debugging. And if a core panic happens in a production
system and the application support any kind of telemetry, it should probably be set up to automatically send a
bug report.
