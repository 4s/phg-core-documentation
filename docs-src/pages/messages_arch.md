
4S PHG Core Messages Architecture     {#page_messages_arch}
=================================

\if false

    ******************************************************************************
    ***                                                                        ***
    ***     This file is part of the 4S PHG core documentation, and contains   ***
    ***     the page/chapter with the messaging architecture.                  ***
    ***                                                                        ***
    ***        AUTHOR:   Jacob Andersen (C) 2020 4S / The Alexandra Institute  ***
    ***        LICENSE:  Apache 2.0                                            ***
    ***                                                                        ***
    ******************************************************************************
    
\endif


[TOC]

The communication between modules in a PHG core system is based on a single shared message bus that all
modules connect to, and a shared "language", which is the subject of this
\htmlonly
page.
\endhtmlonly
\latexonly
chapter.
\endlatexonly


Interfaces and Roles
====================

For communication to take place between PHG core modules, an *interface* must be defined. Interfaces are
defined with two mutually compatible roles, a *producer* and a *consumer*. A module implementing the
producer-end of an interface will be able to communicate with all modules implementing the consumer-end and
vice-versa. Thus, interfaces are arranged as dipoles --- like magnets opposites attract, so a producer cannot
communicate with another producer. Currently, no monopoles can exist (interface capable of communicating with
other interfaces of the same type as itself), but this may be added in the future.

The way the interface roles are defined is completely symmetric. A producer role can "do" exactly the same as
a consumer role. Hence, it is up to the designer of the interface to decide what these roles mean for the
particular interface. The terms 'producer' and 'consumer' suggest that one module offers some service to the
other. This is expected to be the case in most situations, and then the interfaces should be named
accordingly.

A concrete instance of a role of an interface is called an *interface endpoint*. So, for instance, if a "Time"
interface is defined for exchange of current time, the interface would comprise a *Time producer* and a *Time
consumer*, and a module offering access to a clock would implement a *Time producer endpoint*, while a module
needing to read the clock would implement a *Time consumer endpoint*.

The contents of interfaces are defined using an interface description language defined by Google known as
*Protocol Buffers* --- often shortened to *Protobuf*. With these definitions, parsers and serializers
can be auto-generated in all common programming languages. For more about Protobuf, please look at Google's
developer docs: [developers.google.com/protocol-buffers](https://developers.google.com/protocol-buffers/). In
order to define interfaces for PHG core, a certain structure is necessary which will be unfolded in the \ref
sec_defininginterfaces section below.


Communication Patterns
======================

This messages architecture defines two communication patterns that may be used in the definition of
interfaces:

 * **Events:**  a single message, typically unsolicited, sent without any kind of confirmation. The event may
   be sent to all modules implementing the opposite interface role, or it may be sent to one specific module
 * **Functions:** a pair of messages exchanged between two modules --- a request to execute a function,
   followed by that function returning (at least) one response, which could be an error. Note, that it is
   possible to define a function that may return multiple responses. A boolean flag is used to indicate when
   the last response is transmitted.
   
Compared to the 4S Health & Fitness Server microservice architecture, these two communication patterns are
roughly equivalent to HFS messages on the Kafka bus and synchronous REST requests between microservices.


Message Contents
================

At the lower level, messages are exchanged on the message bus. All messages exchanged between modules comprise
two fragments: *header* and *payload*.


Header
------

The header contains all the information needed by the baseplate messaging subsystem to dispatch the message to
its intended recipient(s) and there parse it and deliver the result to the appropriate registered handler.

Three addressing modes are available for messages:

 * **Unicast:** The header must contain the *Peer* address (see below) of the destination module. This
   addressing mode is used to implement the Function communication pattern, and Events may also use this
   addressing mode.
 * **Multicast:** The header must contain a subject matching a subscription at the receiver end. This
   addressing mode is used to implement interface-defined events that are sent from one interface endpoint to
   all modules implementing the interface endpoint of the opposite role.
 * **Broadcast:** This addressing mode is only used for internal baseplate communication --- when the
   `MasterModule` publishes state changes to all modules (`ModuleBase` instances).

For further details on the header format, see dk.s4.phg.baseplate.internal.Header.


Payload
-------

As mentioned earlier, all messages exchanged between modules will carry a payload fragment defined using
Protobuf (see [developers.google.com/protocol-buffers](https://developers.google.com/protocol-buffers/)). The
serializers and parsers for the payload are generated using Protobuf for all the required languages.


Common types
------------

A few common types are defined by the baseplate and are essential to the interface definitions.

### `Peer`

Each module instance is identified by a unique `Peer` value, which is used in messages for source and
destination address. The value must be treated as an opaque identifier. It is generated by the baseplate
during `ModuleBase` construction as a PHG core system-wide unique value.

### `ObjId`

This datatype is used as a PHG core system-wide unique value that may be used much like a UUID value would be
used. It is generated using the `ModuleBase` `createObjId()` member function. An `ObjId`-value is typically
created to identify an instance of something that needs to be communicated between modules in the PHG core
system. More on this below.

### `Error`

The `Error` type was introduced in the \ref sec_errorhandling section. It is used as failure response to
function requests. This resembles a function's ability to throw an exception rather than returning a
value. When used in this way, the conditional 'interface name' and 'error code' properties are mandatory, and
the error code used must be documented with the interface's function definition.


Manipulating Remote Objects
===========================

Using the common types listed in the previous section along the function communication pattern, opens a way to
remotely perform operations on objects --- from one module to another. This kind of manipulation is commonly
known as 'remote method invocation'.

Objects are communicated between modules using a pair of a *handler* (of type `Peer`) and *handle* (of type
`ObjId`). Along with implicit knowledge of the type of the object represented by this (*handler*, *handle*)
pair, it is possible to manipulate the object residing in the *handler* module by sending function requests to
that module which then has the *handle* value as one of its arguments.


Defining Interfaces              {#sec_defininginterfaces}
===================

An interface shall be defined using a single `.proto` file containing Google Protocol Buffers definitions
according to the profile outlined in this section. All interface definition files should begin in the same
way, so as an example, we will define an interface called "FooBar" in a file, `FooBar.proto` containing a few
events and functions:

~~~{.proto}
syntax = "proto3";

package s4.messages.interfaces.foo_bar;
option java_package = "dk.s4.phg.messages.interfaces.foo_bar";
option java_multiple_files = true;
option optimize_for = LITE_RUNTIME;

/**
 * The FooBar interface. Lorem ipsum dolor sit amet, consectetur 
 * adipiscing elit, sed do eiusmod tempor incididunt ut labore et
 * dolore magna aliqua.
 */

// **********************************************************************
//
// Errors defined in this interface
//
// | Error code | Title and short description                           |
// |------------|-------------------------------------------------------|
// |     1      | Invalid or missing argument                           |
// |            | A mandatory argument is missing or not valid          |
// |------------|-------------------------------------------------------|
//
// **********************************************************************



message C2P {
  message Request {
    oneof request {
      OpenFoo open_foo = 1;
      ReadFooBar read_foo_bar = 2;
    }
  }
  message Event {
    oneof event {
      AnnounceBaz announce_baz = 1;
    }
  }
  oneof payload {
    Request request = 1;
    Event event = 2;
  }
}

message P2C {
  message Event {
    oneof event {
      SayHelloBaz say_hello_baz = 1;
    }
  }
  oneof payload {
    // No requests defined at this point
    Event event = 2;
  }
}
~~~

The file shall continue to define the protobuf messages `OpenFoo`, `OpenFooSuccess`, `ReadFooBar`,
`ReadFooBarSuccess`, `AnnounceBaz`, and `SayHelloBaz`, each of which define the actual payload. These
definitions are just "ordinary" protobuf stuff, and therefore left to the imagination of the reader.

Note the use of *PascalCase* and *underscore_case* in this example. This use must be strictly adhered to,
otherwise the baseplate will fail to use the interface definition. The filename must be the PascalCase
`FooBar.proto` while the package name is underscore_case `foo_bar`. Similarly the payload types must be
PascalCase (e.g. `AnnounceBaz`), while the field holding this payload type *shall* be named using the
equivalent underscore_case name (in this example `announce_baz`).

The following paragraphs walks through the central elements of this example.

Package and Options
-------------------

~~~{.proto}
syntax = "proto3";

package s4.messages.interfaces.foo_bar;
option java_package = "dk.s4.phg.messages.interfaces.foo_bar";
option java_multiple_files = true;
option optimize_for = LITE_RUNTIME;
~~~

The package defines the namespace used in JavaScript and C++ (replacing '.' by '::'). So this protobuf file
will generate C++ parser and serializer in the `s4::messages::interfaces::foo_bar` namespace. For Java there
is a special and slightly longer package path. The options are fixed and should not be changed.

Interface Error Definitions
---------------------------

~~~{.proto}
// Errors defined in this interface
//
// | Error code | Title and short description                           |
// |------------|-------------------------------------------------------|
// |     1      | Invalid or missing argument                           |
// |            | A mandatory argument is missing or not valid          |
// |------------|-------------------------------------------------------|
~~~

Each interface controls its own scope of error codes --- typically exceptions that can be thrown out of
function calls. The total list of error codes should appear in a comment at the beginning of the file, and
also for each function request that use them.

Requests and Events Definitions
-------------------------------

~~~{.proto}
message C2P {
  message Request {
    oneof request {
      OpenFoo open_foo = 1;
      ReadFooBar read_foo_bar = 2;
    }
  }
  message Event {
    oneof event {
      AnnounceBaz announce_baz = 1;
    }
  }
  oneof payload {
    Request request = 1;
    Event event = 2;
  }
}
~~~

As explained earlier, an interface defines the communication between two roles, the *producer* and the
*consumer*. This is realized by defining two protobuf messages: a `C2P` protobuf message defining the events
and requests flowing from Consumer-to-Producer, and a `P2C` protobuf message defining the events and requests
flowing from Producer-to-Consumer.

For each event (e.g. `announce_baz`), a protobuf message (`AnnounceBaz` in this example) *shall* be defined
with all the arguments of that event.

For each function (e.g. `read_foo_bar()`), a protobuf message (`ReadFooBar` in this example) *shall* be
defined to hold the arguments of the function request, **and** a protobuf message of the same name plus the
`Success`-suffix (`ReadFooBarSuccess` in this example) *shall* be defined to hold the function response
(return value).


Implementing Endpoints
======================

As explained in the \ref sec_modulebase section, a PHG core module exposes one public class which extends the
`ModuleBase` class of the baseplate. This `ModuleBase`-derived class is responsible for setting up all
interface endpoints in its constructor *before* invoking the mandatory `ModuleBase.start()` function, which
must be invoked before the constructor returns.

For each interface endpoint an `InterfaceEndpoint` object is created using one of the
`ModuleBase.implementsConsumerInterface()` and `ModuleBase.implementsProducerinterface()` member functions. On
this `InterfaceEndpoint` a handler is added for each of the events and functions defined in the `.proto`
file. It is a (runtime) error if a function or an event is defined in the `.proto` file and a corresponding
handler is *not* set up!

The following code snippet illustrates how this can be done in Java (using the JVM baseplate) for a module
implementing a producer for the FooBar interface defined above. The other baseplates work in the same way.

~~~{.java}
import dk.s4.phg.baseplate.*;
import dk.s4.phg.messages.interfaces.foo_bar.*;

public class DemoModule extends ModuleBase {
    public DemoModule(Context context) {

        // Create the base class
        super(context, "Demo");

        // Create the producer interface endpoint and connect handlers
        endpoint = implementsProducerInterface("FooBar");

        endpoint.addFunctionHandler("OpenFoo",
            new FunctionImplementation<OpenFoo, OpenFooSuccess>() {
                public void apply(OpenFoo args,
                                  Callback<OpenFooSuccess> callback,
                                  MetaData meta) {
                    // [...]
                }
            });
        endpoint.addFunctionHandler("ReadFooBar",
            new FunctionImplementation<ReadFooBar, ReadFooBarSuccess>() {
                public void apply(ReadFooBar args,
                                  Callback<ReadFooBarSuccess> callback,
                                  MetaData meta) {
                    // [...]
                }
            });
        endpoint.addEventHandler("AnnounceBaz",
            new EventImplementation<AnnounceBaz>() {
                public void accept(AnnounceBaz args, MetaData meta) {
                    // [...]
                }
            });

        // Start the module
        start();
    }
}
~~~



\if false

Snitflader
 - Et modul erklærer, at det implementerer enten “producer” eller “consumer” siden af en snitflade.
 - Et modul, der erklærer at det er “producer” af snitflade Foo
   - skal subscribe på og håndtere alle multicasts på topic Foo.C2P, der indeholder en s4.messages.foo.C2P
     protobuf-besked med et payload af typen C2P.Event. (Payload af typen C2P.Request er ulovlig og skal logges som en
     fejl, hvis det nogensinde modtages som multicast).
   - skal håndtere (og for requests besvare) alle unicasts på topic Foo.C2P, der indeholder en s4.messages.foo.C2P
     protobuf-besked. (En request payload uden ‘callback’ defineret i topic er ulovlig og skal logges som en fejl.)
   - må sende en besked af typen s4.messages.foo.P2C med payload af typen P2C.Event som multicast eller unicast til en
     modtager, der implementerer consumer Foo.
   - må sende en besked af typen s4.messages.foo.P2C med payload af typen P2C.Request som unicast-med-callback til en
     modtager, der implementerer consumer Foo; og skal i så fald implementere to callbacks, der håndterer et eller flere
     responses af såvel den generiske error type (error_t), henholdsvis af typen s4.messages.foo.FooBarSuccess hvis
     indholdet af den originale request havde typen FooBar.
 - Et modul, der erklærer at det er “consumer” af snitflade Foo skal 
   - subscribe på og håndtere alle multicasts på topic Foo.P2C, der indeholder en s4.messages.foo.P2C protobuf-besked
     med et payload af typen P2C.Event. (Payload af typen P2C.Request er ulovlig og skal logges som en fejl, hvis det
     nogensinde modtages som multicast).
   - håndtere (og for requests besvare) alle unicasts på topic Foo.P2C, der indeholder en s4.messages.foo.P2C
     protobuf-besked. (En request payload uden ‘callback’ defineret i topic er ulovlig og skal logges som en fejl.)
   - må sende en besked af typen s4.messages.foo.C2P med payload af typen C2P.Event som multicast eller unicast til en
     modtager, der implementerer producer Foo.
   - må sende en besked af typen s4.messages.foo.C2P med payload af typen C2P.Request som unicast-med-callback til en
     modtager, der implementerer producer Foo; og skal i så fald implementere to callbacks, der håndterer et eller flere
     responses af såvel den generiske error type (error_t), henholdsvis af typen s4.messages.foo.FooBarSuccess hvis
     indholdet af den originale request havde typen FooBar. 
 - En snitflade definerer sin egen serie af fejlkoder (error_code) til brug med Error beskedtypen.
   - Alle funktioner skal (i dokumentationen) erklære præcis hvilke af disse fejlkoder, de kan returnere med error
     callbacken.
   - Alle funktioner og events, der eksplicit benytter Error typen i argumenter, skal ligeledes dokumentere de mulige
     fejlkoder.
   - Dette giver mulighed for en “try {} catch () {}” lignende konstruktion på tværs af moduler med mulighed for at
     “stack-trace” et problem på tværs af moduler.
   - Det er ikke tilladt over en snitflade at sende en Error besked uden fejlkode, ej heller med en fejlkode, som ikke
     er dokumenteret som anvist ovenfor.

\endif
