
Background     {#page_background}
==========

\if false

    ******************************************************************************
    ***                                                                        ***
    ***     This file is part of the 4S PHG core documentation, and contains   ***
    ***     the page/chapter with the architecture background.                 ***
    ***                                                                        ***
    ***        AUTHOR:   Jacob Andersen (C) 2018 4S / The Alexandra Institute  ***
    ***        LICENSE:  Apache 2.0                                            ***
    ***                                                                        ***
    ******************************************************************************

\endif


[TOC]

The term *Personal Health Gateway* (PHG) --- sometimes also known as an *Application Hosting Device* (AHD) ---
describes a device used to collect data for clinical use from a citizen typically at home or in other private
settings. The PHG will forward the data to a *Health and Fitness Server* (HFS), where the clinician can access
it. Types of data that may be collected by PHGs include:

 * Clinical measurements from home-health devices used in a *Remote Patient Monitoring* situation (such as
   thermometers, blood-pressure meters, glucometers etc.)
 * Fitness data from exercise equipment (tread mills, training bikes, activity trackers etc.)
 * "Independent living" data from sensors in the home (detecting the activity level and general wellbeing of
   the resident)
 * Answers to questionnaires assigned to the citizen by a clinician (also known as *Patient Reported
   Outcomes*)

A PHG can be an application running on a smartphone, tablet, PC, or smartwatch. It can also be a dedicated
device (such as a set-top box) or a feature included in the home Internet router provided by an Internet
Service Provider. And in the future it might be embedded in intelligent clothing, or be a function offered by
smart home systems.

The communication between the different sensor devices listed above --- known collectively as *Personal Health
Devices* (PHDs) --- and a PHG, as well as the communication between a PHG and the HFS backend has been
standardized by the Personal Connected Health Alliance ([www.pchalliance.org](http://www.pchalliance.org))
through the *Continua Design Guidelines* (CDG)\cite cdg.

The overall purpose of the 4S PHG library is to offer a general purpose implementation of these international
standards in order to ease the development of interoperable products that will include the PHG functionality
outlined above, and 
\htmlonly
this page
\endhtmlonly
\latexonly
this chapter
\endlatexonly
will explain the rationale behind the design choices and architecture of the 4S PHG library. 


Strategic Goals
===============

The 4S PHG library was initially created in response to needs and difficulties identified in several Danish
tele-medicine projects implementing Remote Patient Monitoring and Patient Reported Outcomes for different
patient groups. In parallel to these projects a national *Reference Architecture for Collecting Health Data
From Citizens* (\cite refarkEN and \cite refarkDK), which pointed to the Continua Design Guidelines.

The challenges are addressed by the following goals:

 * Based on (International) Standards
 * Multiple Providers
 * Regulatory Compliance and CE Marking
 * Bring-Your-Own-Device
 * Challenges of Rapid Platform Changes


Based on (International) Standards
----------------------------------

Because there is a key difference between compatibility/connectivity and *interoperability*. In the future we
want to be able to combine equipment from many (international) vendors seamlessly.


Multiple Providers
------------------

Several Danish public procurers have adopted a strategy on leveraging the open source licensing models for
more provider independence. Rather than one (big) provider of a full system, they may have several smaller
providers working in parallel on different parts of the system. Furthermore, at any time they may easily
switch to a cheaper or better provider.


Regulatory Compliance and CE Marking
------------------------------------

Some of the uses of PHG software falls within the scope of the “software-as-medical-device” (SaMD) regulation,
which means that it is subject to the Medical Devices Regulation and must have the appropriate CE-marking.

The resolution to these issues will not be covered in detail here, please look at the 4S model for quality
assurance \cite kvalitetssikring (in Danish only). The relevant brief summary is that the library must be
partitioned in independent self-contained modules that cannot interfere with the operation of other modules.


Bring-Your-Own-Device
---------------------

The dream scenario of many telemedicine projects is to develop an application, which the patient/citizen can
download and use on his own device. This is in contrast to many current projects where preinstalled tablets
are handed out to the patients/citizens. Furthermore, many citizens would prefer installing an app on their
own device rather than having to carry an extra piece of equipment around.


Challenges of Rapid Platform Changes
------------------------------------

Banks and insurance companies (among others) often still use mainframe software originally written half a
century ago for their backend platforms. In comparison, it would be impossible to require that a citizen
should use software written for a 10 year-old platform (say Windows XP/7 or iOS version 1). The technology of
the PHG is moving a lot faster (and more importantly *deprecated* a lot faster), than the technology of the
Health and Fitness Server, so the software written for the PHG must be prepared for much more rapid changes in
the underlying hardware and operating system –-- and the emergence of new types of platforms.


Special PHG Concerns
--------------------

Comparing to the 4S Health and Fitness Server microservices, the first three goals are the same, while the two
latter are unique to the 4S PHG library.

The reason for this difference is that on the PHG we have little or no control of the development of the
underlying platform on which our PHG is going to execute. Technology is developing so fast, that the platform
on which the PHG is going to execute will almost certainly be different than the platform the architects
originally envisioned –-- this is in contrast to the HFS where we can always (using appropriate virtualization
tools) create the environment/platform we want, and furthermore in doing so, we rarely have any serious
restrictions with regard to resources such as memory or power.


Architectural Principles
========================

The impacts of the goals listed above on the PHG architecture is further outlined in the report *4S Personal
Health Gateway modules overall design considerations*\cite phg_overall_design. In short, the following
principles are identified as key to the architecture

 1. Independent and self-contained modules specified according to the *single responsibility* principle 
    \cite singleresponsibility, deployed in a "microservices light"-style environment.
 2. Common message bus and messaging style/format capable of segregating modules (e.g. to isolate critical
    functionality), and hide threading complexities.
 3. Based on (international) standards and content formats.
 4. Provide platform abstraction and portability
 5. Minimize the number of different languages and third party dependencies.

Please consult \cite phg_overall_design for the detailed reasoning behind these principles.
