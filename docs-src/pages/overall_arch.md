
4S PHG Overall Architecture     {#page_overall_arch}
===========================

\if false

    ******************************************************************************
    ***                                                                        ***
    ***     This file is part of the 4S PHG core documentation, and contains   ***
    ***     the page/chapter with the overall architecture.                    ***
    ***                                                                        ***
    ***        AUTHOR:   Jacob Andersen (C) 2020 4S / The Alexandra Institute  ***
    ***        LICENSE:  Apache 2.0                                            ***
    ***                                                                        ***
    ******************************************************************************

\endif


[TOC]

The purpose of 
\htmlonly
this page
\endhtmlonly
\latexonly
this chapter
\endlatexonly
is to offer a bird's perspective overview of the entire 4S PHG architecture --- including the "non-core"
components and their relations to the PHG core.


Designed for Portability
========================

The main principle guiding the architecture at this top level is the principle of portability and platform
abstraction (see the [background](\ref page_background)
\htmlonly
page).
\endhtmlonly
\latexonly
chapter).
\endlatexonly
At this level, this principle has two implications:

 * Separate the "business functionality" from platform adaptations.
 * Treat modules closely linked to graphical user interfaces differently than more general functions


Business Functionality Separate from Platform Adaptation
--------------------------------------------------------

In order to maximize the amount of essential code re-use --- minimizing the effort and testing required when
porting the library to new targets --- we distinguish between **platform specific modules** and **platform
independent modules**.

The *platform independent modules* must be created in a portable way that makes no (unnecessary) assumptions
about the target platform (hardware, operating system, available ressources, development environment
etc.). These modules implement the business functionalities and all the high-risk features.

The *platform specific modules* must be as small and "thin" as reasonably possible. These modules offer all
the platform adaptation needed by the platform independent modules, but they should never implement business
functionality or other functionalities associated with a high risk. On the other hand, platform specific
modules *are* allowed to make assumptions on the platform (of course), threading model, development
environment, tooling (e.g. Apple Xcode for iOS-specific modules) and so on.


GUI-related Modules
-------------------

In order to be able to create modules intended for use with graphical user interfaces (such as the "model"
component of a model-view-controller combo) in a way, that is both reasonably portable, and at the same time
reasonably familiar to the GUI developer, we make some further assumptions on this type of application:

> *For platforms powerful enough to drive a graphical user interface, we assume that it is possible (and not
> unreasonable) to implement the GUI components using web technologies --- HTML, CSS, JavaScript.*

For this reason, it is natural to implement these modules in JavaScript (or any language transpiled to
JavaScript, such as TypeScript), such that the GUI application developer will be faced with a familiar
JavaScript API. 

Note, however, that in JavaScript there is just a single event thread driving all modules. Consequences of
this design choice include that these GUI-related modules:

 * cannot perform timing-critical functions,
 * might not be executed in the background --- on many platforms the code driving a website can only be
   expected to execute, when the associated GUI elements are visible on the screen.
 * should avoid many high-risk tasks as these modules are hard to logically isolate from the remaining
   application, and therefore risk management / mitigation and quality assurance of these modules may become
   more difficult and complex.

\latexonly
\begin{figure}\centering
\includegraphics[page=1,width=.4\textwidth]{toplevel_architecture.pdf}
\caption{\label{fig:toplevel_arch}PHG overall architecture}
\end{figure}
\begin{comment}
% a hack to tell Doxygen to copy the image file
\endlatexonly
\htmlonly
<div class="image" style="float: right; margin-left: 30px;
 margin-bottom: 30px; width: 40%;">
<img style="width: 100%;" src="toplevel_architecture.svg" alt="">
<div class="caption">PHG overall architecture.</div>
</div><div style="display:none; visibility:hidden;">
a hack to tell Doxygen to copy the image file
\endhtmlonly
\image latex toplevel_architecture.pdf
\image html toplevel_architecture.svg
\latexonly
\end{comment}
\endlatexonly
\htmlonly
</div>
\endhtmlonly


The Architecture Seen from Above          {#sec_birdsperspective}
================================

The next principles guiding the architecture at this top level are the principles of a "microservices
light"-style environment and a common message bus. (see the [background](\ref page_background)
\htmlonly
page).
\endhtmlonly
\latexonly
chapter).
\endlatexonly
We therefore introduce this "microservices light" message bus --- for the remainder of this section just
shortened to "the bus".

In order to distinguish between the types of modules and their roles in the overall system, we define the
following language:

 * **PHG *pure core* module**: A platform independent module that *only* interacts with the surroundings
   through "the bus".
 * **PHG *mixed core* module**: A platform-independent module that interacts with other (core) modules through
   the bus, while *also* offering APIs that can be used by an application. The GUI-related modules described
   above would typically fit into this category.
 * **PHG *platform abstraction core* module**: A platform specific module that connects to the bus, and is
   also allowed --- and expected --- to interact with the underlying platform (operating system, services,
   drivers).
 * **PHG *non-core* module**: A module that does not connect to the bus. The module only offers its
   functionality directly to the application through a "conventional" API.

The figure illustrates the typical way an application can use 4S PHG modules from each of these categories. Of
course the app itself may also interact directly with core modules using the bus, if it wishes to do so.

The remaining parts of this documentation will focus on the PHG *core* components as indicated by the
title. Therefore, non-core modules will not be covered at all, and at the moment, mixed core modules are also
left out of these docs. The [next](\ref page_core_arch)
\htmlonly
page
\endhtmlonly
\latexonly
chapter
\endlatexonly
will dive into the details of this "microservices light" architecture and its communication paradigm.

\if false

\todo Bonus information -- the following needs to be incorporated in the text at some point...

Platform-uafhængighed af runtime komponenter
--------------------------------------------

For at sikre platform-uafhængighed af alle platform-uafhængige komponenter, stilles følgende krav til disse:

 * Al platform-afhængig funktionalitet skilles ud i små, simple og genbrugelige komponenter under
   PAL. Komponenter i PAL skal designes efter UNIX-filosofien: Implementer én simpel funktion godt.
 * Komponenter over PAL skal laves, så de kan fungere i alle mulige forskellige tråd-miljøer: Multi-trådede
   miljøer med thread-safe funktionskald, enkelt-trådede miljøer, hvor alle funktionskald skal være korte og
   ikke-blokerende (fx fordi al eksekvering kører på en GUI eventtråd).

Languages...


Eksterne platform-uafhængige komponenter
----------------------------------------

Så vidt muligt undgås afhængigheder af eksterne komponenter med mindre særlige grunde taler for afhængigheden,
og den eksterne komponent i øvrigt overholder licenskravene listet i et tidligere afsnit.

Generelt skal der være så få eksterne afhængigheder som overhovedet muligt, og de afhængigheder, der er, skal
være store/udbredte og stabile/gamle, således at de ikke pludselig forsvinder fra markedet eller ændrer sig
radikalt.

Såfremt en ekstern afhængighed tages ind i 4S PHG runtime, skal den understøtte alle platforme – inklusive
fremtidige, og være neutral i forhold til udviklingsmiljøer / IDE / toolchain.

Compiletime/test afhængigheder skal understøtte de almindelige desktop platforme, som må formodes at blive
brugt til udvikling. De må ikke være låst til bestemte valg af IDE eller udviklingsmiljøer / toolchains, men
skal fungere på tværs af disse (fx Gnu Make/Gcc, Visual Studio, Eclipse, Xcode). Et demo eller tutorial
projekt må dog godt stille krav om et bestemt IDE / udviklingsmiljø.

Platform-specifikke komponenter
------------------------------

Platform-specifikke komponenter må kun forefindes i lagene under PAL (se senere). Her kan der undtagelsesvis
være behov for at fravige fra ovenstående retningslinjer omkring licens, sprog og eksterne komponenter – og
dette kan så ske, såfremt gode argumenter taler for dette. Komponenterne i lagene under PAL har i øvrigt også
lov til at stille særlige krav til udviklingsmiljø/toolchain. Fx kan iOS komponenter forventes at kræve, at
Xcode benyttes til at bygge applikationen, mens Windows Phone komponenter kan forventes at kræve Visual
Studio.

Understøttede platforme

 * Traditionel desktop:  Windows, OSX, Linux
 * Mobil/tablet: Android, iOS, Windows Phone
 * Tynde klienter: ChromeOS, Firefox OS



Eksempler på typiske moduler

 * Device manager
 * Historik over målinger
 * Hjælp til brug af måleapparat (printer-driver analogi)
 * Spørgeskemabesvarelse
 * Videokonference
 * Chat
 * etc.

\endif
