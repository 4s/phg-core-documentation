
\page page_contributors

\if false
******************************************************************************
***                                                                        ***
***     This file is part of the 4S PHG core documentation, and contains   ***
***     the page/chapter with the (legal) messages from the contributors.  ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) 2018 4S / The Alexandra Institute  ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif

\addindex NOTICE
\addindex Contributors

The following sections lists the messages from 4S PHG Core Module
Collection contributors (the `NOTICE` file found in each individual
module).


4S PHG Core Modules Collection Documentation
============================================

\verbinclude "../NOTICE"


4S PHG Native Baseplate
=======================

\verbinclude "../phg-native-baseplate/NOTICE"


4S PHG JavaVM Baseplate
=======================

\verbinclude "../phg-jvm-baseplate/NOTICE"


Monica CTG Module
=================

\verbinclude "../monica-ctg-module/NOTICE"


Bluetooth Android Module
========================

\verbinclude "../bluetooth-android-module/NOTICE"
