# Introduction to PHG Core Documentation #

The purpose of this repository is to collect and compile the
documentation from all PHG core modules into a single web site or PDF
document. The result is intended for publication on
http://www.4s-online.dk/PHG/


### Documentation ###

Two types of documentation are available: the *"interface
documentation"* is aimed at developers using modules from the 4S PHG
collection, and the *"full documentation"* is aimed at developers
working on the modules.


### Building ###

In order to build the documentation, the following tools must be
installed on your system:

- [Doxygen](http://doxygen.org)     (minimum version 1.8.13)

- (pdf)LaTeX                        (any recent distribution)

- [Graphviz](http://graphviz.org)   (tested with version 2.38.0)
 
- [PlantUML](http://plantuml.com/)  (tested with version 8021)

To build the interface documentation for html and pdf, execute the
following two commands (only the first is necessary to build
html-only):

```
> doxygen
> cd docs/interface/latex && make
```

The HTML documentation will now be available at
`docs/interface/html/index.html`, and the PDF document at:
`docs/interface/latex/refman.pdf`.

To build the full documentation for html and pdf, execute the
following two commands (only the first is necessary to build
html-only):

```
> doxygen Doxyfile-full
> cd docs/full/latex && make
```

The HTML documentation will now be available at
`docs/full/html/index.html`, and the PDF document at:
`docs/full/latex/refman.pdf`.


### Issue tracking ###

Issue tracking for this repository can be found at:
https://issuetracker4s.atlassian.net/browse/PM


### License ###

The source files are released under Apache 2.0, you can obtain a copy
of the License at: http://www.apache.org/licenses/LICENSE-2.0

Documentation text in general is under Creative Commons Attribution
4.0 (a.k.a CC-BY-4.0), you can obtain a copy of this license at
http://creativecommons.org/licenses/by/4.0/